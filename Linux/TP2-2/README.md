# TP2 PARTIE 2
## I. Monitoring
### 2. SetUp
**SetUp Netdata**
```bash
# Passez en root pour cette opération
$ sudo su -

# Install de Netdata via le script officiel statique
\# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)

# Quittez la session de root
$ exit
```

**Manipulation du *service* Netdata**
```bash
# Déterminer si le service et actif et se boot bien au démarrage du système
[maximauve@web ~]$ sudo systemctl is-active netdata
active
[maximauve@web ~]$ sudo systemctl is-enabled netdata
enabled

# Déterminer sur quel port netdata est en écoute
[maximauve@web ~]$ sudo ss -alnpt | grep "netdata"
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2300,fd=35))

LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2300,fd=5))

LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2300,fd=34))

LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2300,fd=6))

# Modification du firewall
[maximauve@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[maximauve@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[maximauve@web ~]$ sudo firewall-cmd --reload
success
[maximauve@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**SetUp Alerting**

```bash
# Ajuster la conf de Netdata pour mettre en place des alertes discord
[maximauve@web ~]$ cat /opt/netdata/etc/netdata/health_alarm_notify.conf
[...]
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897146477598605343/efktBOH4LHpAkJv6Gjcw1nHTNpvcV09owSCEjCUzsaUIUQyw_I-Xb67wZ_LlhV7bUe4e"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
[...]

# Vérifier le bon fonctionnement de l'Alerting
[maximauve@web ~]$ sudo su -s /bin/bash netdata
[sudo] password for maximauve:
bash-4.4$
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
[...]
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-11 18:01:32: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
\# OK

[maximauve@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

**Config alerting**





## II. Backup
### 2. Partage NFS
**SetUp environnement**

```bash
[maximauve@backup ~]$ sudo mkdir /srv/backup/
[maximauve@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux/
[maximauve@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux/
```
**SetUp partage NFS**

```bash
#Install de nfs
[maximauve@backup ~]$ sudo dnf -y install nfs-utils

# ajout du nom de domaine dans le fichier conf
[maximauve@backup ~]$ sudo vim /etc/idmapd.conf
[maximauve@backup ~]$ cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux

# ajout des chemins de partage nsf
[maximauve@backup ~]$ sudo vim /etc/exports
[maximauve@backup ~]$ cat /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11/24(rw)
/srv/backup/db.tp2.linux 10.102.1.12/24(rw)

# Activation du service
[maximauve@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

# Autorisation auprès du firewall
[maximauve@backup ~]$ sudo firewall-cmd --add-service=nfs
success
[maximauve@backup ~]$ sudo firewall-cmd --runtime-to-permanent
success
[maximauve@backup ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: nfs ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**Setup points de montage sur `web.tp2.linux`**

```bash
# Install de NFS
[maximauve@web ~]$ sudo dnf -y install nfs-utils

# Changement du nom de domaine dans le fichier de conf
[maximauve@web ~]$ sudo vim /etc/idmapd.conf
[maximauve@web ~]$ cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux

# Création du dossier de backup
[maximauve@web ~]$ sudo mkdir /srv/backup/

# Montage du dossier de la vm backup sur la vm web
[maximauve@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup
# Je regarde si elle est bien montée
[maximauve@web ~]$ mount | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=262144,wsize=262144,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)

# Je regarde s'il y a bien de la place dessus
[maximauve@web ~]$ df -hT
Filesystem                                 Type      Size  Used Avail Use% Mounted on
devtmpfs                                   devtmpfs  891M     0  891M   0% /dev
tmpfs                                      tmpfs     909M  332K  909M   1% /dev/shm
tmpfs                                      tmpfs     909M  8.6M  901M   1% /run
tmpfs                                      tmpfs     909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root                        xfs       6.2G  3.8G  2.4G  62% /
/dev/sda1                                  xfs      1014M  241M  774M  24% /boot
tmpfs                                      tmpfs     182M     0  182M   0% /run/user/1000
backup.tp2.linux:/srv/backup/web.tp2.linux nfs4      6.2G  2.0G  4.3G  32% /srv/backup



```
