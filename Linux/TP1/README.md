# TP Linux 1

## 0. Préparation de la machine

### Setup de deux machines Rocky Linux configurée de façon basique.

- Un accès internet avec carte NAT
  On va tout d'abord regarder ma carte NAT, ensuite nous allons ping `8.8.8.8` et `google.com`
  VM1:

```
[maximauve@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
[maximauve@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.8 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 18.752/19.693/20.635/0.951 ms
[maximauve@node1 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=27.5 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=116 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 27.472/71.535/115.598/44.063 ms
```

VM2:

```
[maximauve@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
[maximauve@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=26.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=29.7 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 26.549/28.135/29.721/1.586 ms
[maximauve@node2 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=32.4 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=63.4 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 32.395/47.900/63.405/15.505 ms
```

Tout fonctionne correctement, je peux continuer.

- Un accès à un réseau local
  Tout d'abord, on commence par définir des adresses IP statiques pour nos cartes Host-Only.
  Nous devons donc modifier le fichier suivant:
  `/etc/sysconfig/network-scripts/ifcfg-enp0s8`
  On utilise `sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`
  Nous modifions les valeurs `BOOTPROTO=dhcp` à `BOOTPROTO=static` et `ONBOOT=no` à `ONBOOT=yes`
  Ensuite, on ajoute `IPADDR`, `NETMASK` et `DNS1`
  Cela nous donne ceci:

```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=df32e67c-a050-4fae-9dc3-660ac7f44bc8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
DNS1=1.1.1.1
```

Une fois fait, on redémarre l'interface:
`sudo nmcli con reload`
`sudo nmcli con up enp0s8`

On voit maintenant qu'en faisant `ip a` nous avons notre carte réseau activée avec son adresse ip
Vm1:

```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7d:4d:93 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.10.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7d:4d93/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Vm2:

```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:32:e2:92 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.10.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe32:e292/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Nos deux machines peuvent maintenant se ping entre elles:
Vm1:

```
[maximauve@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.579 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.80 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=1.73 ms
^C
--- 10.101.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 0.579/1.369/1.795/0.559 ms
```

Vm2:

```
[maximauve@node2 ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=1.10 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.850 ms
64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=0.544 ms
^C
--- 10.101.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2027ms
rtt min/avg/max/mdev = 0.544/0.829/1.095/0.227 ms
```

- Changer le nom d'utilisateur

Pour le changer directement, on utilise la commande `sudo hostname` avec "node1.tp1.b2" pour la 1ère VM et node2.tp1.b2" pour la seconde.

Pour changer le nom d'utilisateur pour les prochains ulumages, on utilise la commande `echo "<username>" | sudo tee /etc/hostname` où `<username>` est "node1.tp1.b2" pour la première VM et "node2.tp1.b2" pour la seconde VM.

La commande `tee` permet de mettre le résultat de la commande précédente (`echo`) dans un fichier. Elle remplace le chevron et on peut l'utiliser avec `sudo` pour des fichiers restreints d'accès.

on voit que nos nom d'utilisateurs sont maintenant bien changés.

```
[maximauve@node1 ~]$ hostname
node1.tp1.b2
```

```
[maximauve@node2 ~]$ hostname
node2.tp1.b2
```

- DNS
  Je vérifie que le serveur DNS dans le fichier `/etc/resolv.conf` soit bien à `1.1.1.1`
  VM1:

```
[maximauve@node1 ~]$ sudo cat /etc/resolv.conf
[sudo] password for maximauve:
# Generated by NetworkManager
search auvence.co tp1.b2
nameserver 1.1.1.1
```

VM2:

```
[maximauve@node2 ~]$ sudo cat /etc/resolv.conf
[sudo] password for maximauve:
# Generated by NetworkManager
search auvence.co tp1.b2
nameserver 1.1.1.1
```

C'est le cas, on continue.

- Donner un "nom de contact" aux machines:
  Pour se faire, on modifie le fichier /etc/hosts avec `nano` et on rajoute un ligne avec l'adresse IP suivie du nom de l'autre machine dans le fichier.
  VM1:

```
10.101.1.12 node2.tp1.b2
```

VM2:

```
10.101.1.11 node1.tp1.b2
```

On regarde maintenant si les deux machines peuvent toujours se ping:
VM1:

```
[maximauve@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.745 ms
^C
--- node2.tp1.b2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.745/0.745/0.745/0.000 ms
```

VM2:

```
[maximauve@node2 ~]$ [maximauve@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.682 ms
^C
--- node1.tp1.b2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.682/0.682/0.682/0.000 ms
```

- Configuration du firewall
  J'utilise la commande `sudo firewall-cmd --list-all` et j'obtiens le retour suivant sur mes deux VM:

```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Le ssh est activé, c'est good.

## I. Utilisateurs

### 1. Création et Configuration

- Ajouter un utilisateur à la machine:

J'utilise la commande `sudo useradd <user> -m -s /bin/bash` où `user` est le nom que je choisis pour mon user, `-m` crée le répertoire dans le /home et `-s /bin/bash` qui précise le shell par défaut.
Je regarde ensuite s'ils se trouvent bien dans mon /home:

VM1:

```
[maximauve@node1 ~]$ sudo useradd user1 -m -s /bin/bash
[maximauve@node1 ~]$ ls /home
maximauve  user1
```

VM2:

```
[maximauve@node2 ~]$ sudo useradd user2 -m -s /bin/bash
[maximauve@node2 ~]$ ls /home
maximauve  user2
```

- Créer un groupe admin:

On utilise `sudo groupadd admins` sur les deux VMs
Puis on va modifier les perms dans le fichier /etc/sudoers grâce à `sudo visudo /etc/sudoers`
Je rajoute la ligne suivante:

```
%admins ALL=(ALL)    ALL
```

- Ajouter notre utilisateur au groupe `admins`:

La commande `sudo usermod -aG admins <user>` où `-aG` signifie d'ajouter l'utilisateur au groupe donné, sans retirer les autres groupes dont il fait parti et `user` est l'utilisateur selectionné.
J'utilise ensuite la commande `groups` pour voir si mon user a bien été ajouté au groupe.

VM1:

```
[maximauve@node1 ~]$ sudo usermod -aG admins user1
[maximauve@node1 ~]$ groups user1
user1 : user1 admins
```

VM2:

```
[maximauve@node2 ~]$ sudo usermod -aG admins user2
[maximauve@node2 ~]$ groups user2
user2 : user2 admins
```

### 2. SSH

Je crée d'abord ma clé sur mon ordinateur:

```
C:\Users\maxmo\.ssh>ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\maxmo/.ssh/id_rsa): C:\Users\maxmo/.ssh/id_rsa_Linux
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\maxmo/.ssh/id_rsa_Linux.
Your public key has been saved in C:\Users\maxmo/.ssh/id_rsa_Linux.pub.
The key fingerprint is:
SHA256:qJbJRuT747wlepuPdBCd0taH4PNgNN7g4+TaRzuxIgU maxmo@PC-de-Maximauve
The key's randomart image is:
+---[RSA 4096]----+
|        =        |
|       B B .     |
|    . E # + .    |
|   o   @ = .     |
|    o o S +      |
|   o = = . +     |
|    O = = =      |
|   o =oB o .     |
|    .oO=.        |
+----[SHA256]-----+
```

Comme je possède déjà une clé avec le nom qu'il me proposait par défaut, j'ai explicitement demandé un autre nom.

Maintenant, Je vais la déposer sur mes VMs:
Je crée d'abord un dossier `.ssh` dans mon `/home/user`, puis je fais `nano authorized_keys` pour créer mon ficher.

Je dois maintenant copier ma clé publique depuis mon ordinateur. Pour ce faire, je fais la commande suivante:

```
C:\Users\maxmo\.ssh>type id_rsa_Linux.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9ErQBswCKcRa3P4JJe0KV4G1LrKdrxV1w5k45K/H6QVIzwJ8hDU1cFD10CK5yMSV0jMFWSjXgonVODTWkSSfUd8aKIDddQOYWuAroock0e3wUo/+9EhsNVbVSpRI7CDaBdpjIeHr7O1D0UnFDSNLnFli4lMo6Z8hS53vQzfMMi42hGLQmIe79ohIYA9Ag7MHRGeCXVQLzgKUCLzL1ema3BQ3H72ntlRkLBmz119Idv2koYNTBNBiTLwvjPXKDXK/JRvB5UDBdinWtGnbAvlKF49xphBnfrXTSWEMsxs+OzSQTQJZkzBjQu6Pq9B+fVsFY/RqtXZlkqLlRQsc7kWOgB1iSX6panSiqJFRrelxswG8RU23ND3NRSG9MCo17WAiJsf7SXROMmkI3lIwWrQSRJB9GpMLr+P4myARIhm4GjvSTeO5QgrK25i9YdaG0KXS/B3Ve/gChXLS7PAYInNsSONwmWmdT0zynvjw+oxnEfk7Oev3raTR4TyOxQjHxRRAD1TqVPc7n81XAgawJWwrY7sM2OVgZnEY1M9Y5zFwMl+VfvvbEkLOZbBrmMc9ooqlcIBJYUFWGQFQT2lukb4kJuGHBJFvatAfvk568lk9taQ/cK1DTbnamPL3isXL1+sYmDpYUHfqA5o09hQe05zuF8o2ZNk8ZrsTAoQp3th0mIQ== maxmo@PC-de-Maximauve
```

Je récupère ma clé, et je viens la coller dans le fichier que je viens de créer dans ma vm.
Je regarde mainteant si elle s'y trouve bien

```
[user1@node1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9ErQBswCKcRa3P4JJe0KV4G1LrKdrxV1w5k45K/H6QVIzwJ8hDU1cFD10CK5yMSV0jMFWSjXgonVODTWkSSfUd8aKIDddQOYWuAroock0e3wUo/+9EhsNVbVSpRI7CDaBdpjIeHr7O1D0UnFDSNLnFli4lMo6Z8hS53vQzfMMi42hGLQmIe79ohIYA9Ag7MHRGeCXVQLzgKUCLzL1ema3BQ3H72ntlRkLBmz119Idv2koYNTBNBiTLwvjPXKDXK/JRvB5UDBdinWtGnbAvlKF49xphBnfrXTSWEMsxs+OzSQTQJZkzBjQu6Pq9B+fVsFY/RqtXZlkqLlRQsc7kWOgB1iSX6panSiqJFRrelxswG8RU23ND3NRSG9MCo17WAiJsf7SXROMmkI3lIwWrQSRJB9GpMLr+P4myARIhm4GjvSTeO5QgrK25i9YdaG0KXS/B3Ve/gChXLS7PAYInNsSONwmWmdT0zynvjw+oxnEfk7Oev3raTR4TyOxQjHxRRAD1TqVPc7n81XAgawJWwrY7sM2OVgZnEY1M9Y5zFwMl+VfvvbEkLOZbBrmMc9ooqlcIBJYUFWGQFQT2lukb4kJuGHBJFvatAfvk568lk9taQ/cK1DTbnamPL3isXL1+sYmDpYUHfqA5o09hQe05zuF8o2ZNk8ZrsTAoQp3th0mIQ== maxmo@PC-de-Maximauve
```

Je viens ensuite mettre les bonnes permissions sur mon fichier `authorized_keys` et mon dossier `.ssh` :

```
[user1@node1 .ssh]$ chmod 600 authorized_keys
[user1@node1 .ssh]$ ls -l
total 4
-rw-------. 1 user1 user1 747 Sep 23 11:26 authorized_keys
[user1@node1 .ssh]$ cd ..
[user1@node1 ~]$ chmod 700 .ssh/
[user1@node1 ~]$ ls -all
total 16
[...]
drwx------. 2 user1 user1  29 Sep 23 11:26 .ssh
```

Toutes mes permissions sont bonnes.

Je vais à présent tenter de me connecter et voir si le mot de passe ne m'est plus demandé:
VM1:

```
C:\Users\maxmo>ssh user1@10.101.1.11 -i .ssh/id_rsa_Linux
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 11:36:56 2021 from 10.101.1.1
[user1@node1 ~]$
```

VM2:

```
C:\Users\maxmo>ssh user2@10.101.1.12 -i .ssh/id_rsa_Linux
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 11:18:43 2021 from 10.101.1.1
[user2@node2 ~]$
```

## II. Partitionnement

### 1. Préparation de la VM

Après avoir ajouté mes deux disques à ma vm, je regarde avec la commande `lsblk` pour voir s'ils sont bien présent:

```
[user1@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```

### 2. Partitionnement

Je commence par ajouter mes deux disques en tant que disques physiques dans LVM

```
[user1@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for user1:
  Physical volume "/dev/sdb" successfully created.

[user1@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.

[user1@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
[user1@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               kbN272-LzEA-AXL3-sAPg-sdvz-n5Jt-OHmHfm

  "/dev/sdb" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               dImQob-i1bB-kxVi-Fm6K-gxRT-9eVJ-i4r5qY

  "/dev/sdc" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               0o61TL-V4EQ-L23i-Zrvq-0xMS-Mgt0-e2h6xz
```

Maintenant, je crée un volume group avec mes deux disques:

```
[user1@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created

[user1@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended

[user1@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   0   0 wz--n-  5.99g 5.99g
  rl     1   2   0 wz--n- <7.00g    0
[user1@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               data
  System ID
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               5.99 GiB
  PE Size               4.00 MiB
  Total PE              1534
  Alloc PE / Size       0 / 0
  Free  PE / Size       1534 / 5.99 GiB
  VG UUID               6aZFLb-OJ1H-LQov-Kq6d-JqUP-evLx-ewvo39
[...]
```

Je crée ensuite les 3 disques logiques de 1Go chacuns

```
[user1@node1 ~]$ sudo lvcreate -L 1G data -n data_1
[sudo] password for user1:
  Logical volume "data_1" created.

[user1@node1 ~]$ sudo lvcreate -L 1G data -n data_2
  Logical volume "data_2" created.

[user1@node1 ~]$ sudo lvcreate -L 1G data -n data_3
  Logical volume "data_3" created.

[user1@node1 ~]$ sudo lvs
  LV     VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data_1 data -wi-a-----   1.00g
  data_2 data -wi-a-----   1.00g
  data_3 data -wi-a-----   1.00g
  root   rl   -wi-ao----  <6.20g
  swap   rl   -wi-ao---- 820.00m

[user1@node1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/data/data_1
  LV Name                data_1
  VG Name                data
  LV UUID                AJBtth-tWjW-p2LQ-51wi-gpIX-Co1V-Mk1uhq
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-23 12:29:14 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/data/data_2
  LV Name                data_2
  VG Name                data
  LV UUID                YxHjUz-ZcmE-IfnU-z181-rxYW-CTmi-O0SafC
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-23 12:29:18 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/data/data_3
  LV Name                data_3
  VG Name                data
  LV UUID                oXDGey-czAL-711X-X8Ql-T1ik-NkHF-I6kFca
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-23 12:29:21 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4

[...]
```

Je les formatte en `ext4`

```
[user1@node1 ~]$ sudo mkfs -t ext4 /dev/data/data_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 84244419-8615-437c-b81b-f47952752fc7
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[user1@node1 ~]$ sudo mkfs -t ext4 /dev/data/data_2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: b6a9c368-9806-4bfa-8182-cbdfe3b9bac4
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[user1@node1 ~]$ sudo mkfs -t ext4 /dev/data/data_3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: c34d9647-bc70-46e2-b91e-bacaebcd9ad2
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

Et enfin, je les monte:

```
[user1@node1 ~]$ sudo mkdir /mnt/part1
[user1@node1 ~]$ sudo mkdir /mnt/part2
[user1@node1 ~]$ sudo mkdir /mnt/part3

[user1@node1 ~]$ sudo mount /dev/data/data_1 /mnt/part1
[user1@node1 ~]$ sudo mount /dev/data/data_2 /mnt/part2
[user1@node1 ~]$ sudo mount /dev/data/data_3 /mnt/part3

[user1@node1 ~]$ mount
[...]
/dev/mapper/data-data_1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-data_2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-data_3 on /mnt/part3 type ext4 (rw,relatime,seclabel)

[user1@node1 ~]$ df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 890M     0  890M   0% /dev
tmpfs                    909M     0  909M   0% /dev/shm
tmpfs                    909M  8.6M  901M   1% /run
tmpfs                    909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root      6.2G  1.9G  4.4G  30% /
/dev/sda1               1014M  182M  833M  18% /boot
tmpfs                    182M     0  182M   0% /run/user/1000
tmpfs                    182M     0  182M   0% /run/user/1001
/dev/mapper/data-data_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-data_2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-data_3  976M  2.6M  907M   1% /mnt/part3
```

Je vais ensuite faire en sorte que les patitions soient montée au boot de la VM avec `sudo nano /etc/fstab`
Et je rentre les 3 lignes suivantes:

```
/dev/data/data_1 /mnt/part1 ext4 defaults 0 0
/dev/data/data_2 /mnt/part2 ext4 defaults 0 0
/dev/data/data_3 /mnt/part3 ext4 defaults 0 0
```

Je démonte mes partitions pour les remonter ensuite:

```
[user1@node1 ~]$ sudo umount /mnt/part1
[user1@node1 ~]$ sudo umount /mnt/part2
[user1@node1 ~]$ sudo umount /mnt/part3
[user1@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounted
```

## III. Gestion de services
