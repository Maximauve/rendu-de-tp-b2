# TP Linux 2

## I. Un premier serveur Web

### 1. Installation

**Installer le serveur Apache**

```
[maximauve@web ~]$ sudo dnf install httpd

[...]
Installed:
  apr-1.6.3-11.el8.1.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
  httpd-filesystem-2.4.37-39.module+el8.4.0+571+fd70afb1.noarch
  httpd-tools-2.4.37-39.module+el8.4.0+571+fd70afb1.x86_64
  mod_http2-1.15.7-3.module+el8.4.0+553+7a69454b.x86_64
  rocky-logos-httpd-84.5-8.el8.noarch

Complete!
```
Je supprime tous les commentaires dans mon fichier avec  `sudo vim /etc/httpd/conf/httpd.conf`:

**Démarrer le service Apache**

```bash
# On le démarre
[maximauve@web ~]$ sudo systemctl start httpd
# On active le démarrage automatique de httpd
[maximauve@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
# On regarde quel port utilise httpd
[maximauve@web ~]$ sudo ss -alnpt
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=754,fd=5))
LISTEN        0             128                              *:80                            *:*           users:(("httpd",pid=24154,fd=4),("httpd",pid=24153,fd=4),("httpd",pid=24152,fd=4),("httpd",pid=24150,fd=4))
LISTEN        0             128                           [::]:22                         [::]:*           users:(("sshd",pid=754,fd=7))
# On voit qu'il utilise le port 80, je vais l'ouvrir avec le firewall
[maximauve@web ~]$ sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
[sudo] password for maximauve:
success
[maximauve@web ~]$ sudo firewall-cmd --reload
success
```

**TEST**

```bash
# Service activé:
[maximauve@web ~]$ sudo systemctl is-active httpd
active
# Service configuré pour démarrer automatiquement:
[maximauve@web ~]$ sudo systemctl is-enabled httpd
enabled
# Vérifier qu'on joint le serveur web localement:
[maximauve@web ~]$ curl localhost
[...]
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>
[...]
 <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
</footer>
[...]
```
J'arrive bien à accéder à mon serveur web depuis mon PC avec l'adresse `10.102.1.11:80`

### 2. Avancer vers la maîtrise du service
**Le service d'Apache**
```bash
# Commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
[maximauve@web ~]$ sudo systemctl enable httpd
# Vérification pour savoir si le service est bien paramétré pour s'activer lorque la machine s'allume
[maximauve@web ~]$ sudo systemctl is-enabled httpd
enabled
# Définition du service apache
[maximauve@web ~]$ sudo systemctl cat httpd
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

**Déterminer sous quel utilisateur tourne le processus Apache**

```bash
# La ligne qui défini quel utlisateur est utilisé
[maximauve@web ~]$ cat /etc/httpd/conf/httpd.conf | grep User
User apache
[...]

# Vérification si apache tourne bien sous l'utilisateur "apache"
[maximauve@web ~]$ ps -ef | grep apache
apache     24151   24150  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24152   24150  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24153   24150  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24154   24150  0 16:49 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

# Vérification que le contenu du dossier /usr/share/testpage/ est bien accessible à l'utilisateur "apache"
[maximauve@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 16:36 .
drwxr-xr-x. 91 root root 4096 Sep 29 16:36 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
# Le dossier est accessible à l'utilisateur, et le fichier présent dedans est lisible.
```

**Changer d'utilisateur Apache**
```bash
# créer un nouvel utilisateur
[maximauve@web ~]$ sudo useradd apache2 -m -s /sbin/nologin

[maximauve@web ~]$ cat /etc/passwd
[...]
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
apache2:x:1001:1001::/home/apache2:/sbin/nologin

# Changer l'utilisateur utilisé 
[maximauve@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[maximauve@web ~]$ cat /etc/httpd/conf/httpd.conf | grep User
User apache2
[...]

# Redémarrer le service
[maximauve@web ~]$ sudo systemctl restart httpd
[maximauve@web ~]$ ps -ef | grep apache2
apache2     1845    1843  0 15:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache2     1846    1843  0 15:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache2     1847    1843  0 15:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache2     1848    1843  0 15:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

**Faites en sorte que Apache tourne sur un autre port**

```bash
# Modifier la configuration d'Apache pour changer le port d'écoute
[maximauve@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[maximauve@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"
[...]

# Modifier le firewall en fonction
[maximauve@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[maximauve@web ~]$ sudo firewall-cmd --add-port=100/tcp --permanent
success
[maximauve@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[maximauve@web ~]$ sudo firewall-cmd --reload
success
[maximauve@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 100/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

# Redémarrer Apache
[maximauve@web ~]$ sudo systemctl restart httpd

# Regarder si les changements sont bien appliqués
[maximauve@web ~]$ sudo ss -alnpt
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=755,fd=5))
LISTEN     0          128                     [::]:22                   [::]:*        users:(("sshd",pid=755,fd=7))
LISTEN     0          128                        *:100                     *:*        users:(("httpd",pid=2135,fd=4),("httpd",pid=2134,fd=4),("httpd",pid=2133,fd=4),("httpd",pid=2130,fd=4))

# Vérifier qu'on peut toujours joindre Apache sur le nouveau port
[maximauve@web ~]$ curl localhost:100
<!doctype html>
<html>
[...]
    <h1>HTTP Server <strong>Test Page</strong></h1>
[...]
```
Je peux toujours bien joindre mon serveur Apache sur mon navigateur à `10.102.1.11:100`

📁`httpd.conf`

### 2. SetUp

#### A. Serveur Web et NetCloud

**Install du Serveur Web et de NextCloud sur `web.tp2.linux`**
```bash
[maximauve@web ~]$ sudo dnf install epel-release
[sudo] password for maximauve:
[...]
Complete!

[maximauve@web ~]$ sudo dnf update
[...]
Complete!

[maximauve@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
Complete!

[maximauve@web ~]$ dnf module list php
[...]
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                                           1.4 MB/s | 2.0 MB     00:01
Rocky Linux 8 - AppStream
Name                       Stream                         Profiles                                        Summary
php                        7.2 [d]                        common [d], devel, minimal                      PHP scripting language
php                        7.3                            common [d], devel, minimal                      PHP scripting language
php                        7.4                            common [d], devel, minimal                      PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                       Stream                         Profiles                                        Summary
php                        remi-7.2                       common [d], devel, minimal                      PHP scripting language
php                        remi-7.3                       common [d], devel, minimal                      PHP scripting language
php                        remi-7.4                       common [d], devel, minimal                      PHP scripting language
php                        remi-8.0                       common [d], devel, minimal                      PHP scripting language
php                        remi-8.1                       common [d], devel, minimal                      PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

[maximauve@web ~]$ sudo dnf module enable php:remi-7.4
[...]
Complete!

[maximauve@web ~]$ dnf module list php
[...]
Name                      Stream                            
[...]
php                       remi-7.4 [e]                      common [d], devel, minimal                     PHP scripting language


[maximauve@web ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Complete!

[maximauve@web ~]$ sudo systemctl enable httpd

[maximauve@web ~]$ sudo mkdir /etc/httpd/sites-available
[maximauve@web ~]$ sudo vi /etc/httpd/sites-available/web.tp2.linux
[maximauve@web ~]$ cat /etc/httpd/sites-available/web.tp2.linux
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>


[maximauve@web ~]$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/

[maximauve@web ~]$ sudo vim /etc/opt/remi/php74/php.ini


[maximauve@web ~]$ cat /etc/opt/remi/php74/php.ini
[...]
date.timezone = "Europe/Paris"
[...]

[maximauve@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[...]
2021-10-06 16:51:34 (10.1 MB/s) - ‘nextcloud-22.2.0.zip’ saved [159315304/159315304]

[maximauve@web ~]$ unzip nextcloud-22.2.0.zip
[...]

[maximauve@web nextcloud]$ sudo mkdir /var/www/sub-domains/
[maximauve@web nextcloud]$ sudo mkdir /var/www/sub-domains/web.tp2.linux/
[maximauve@web nextcloud]$ sudo mkdir /var/www/sub-domains/web.tp2.linux/html/
[maximauve@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/

[maximauve@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
IncludeOptional sites-enabled/*

[maximauve@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/com.yourdomain.nextcloud/html
[maximauve@web nextcloud]$ sudo systemctl restart httpd
```
📁`httpd.conf2`
📁`web.tp2.linux`


#### B. Base de données

**Installation de MariaDB sur `db.tp2.linux`**
```bash
[maximauve@db ~]$ sudo dnf install mariadb-server
[...]
Complete!

[maximauve@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[maximauve@db ~]$ sudo systemctl start mariadb
# Je n'ai pas mis de mot de passe comme ce n'est que moi qui gère cette vm. Pour le reste, j'ai mis yes à tout.
Thanks for using MariaDB!

# Je repère le port utilisé par MariaDB
State    Recv-Q   Send-Q       Local Address:Port       Peer Address:Port   Process
LISTEN   0        128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=751,fd=5))
LISTEN   0        80                       *:3306                  *:*       users:(("mysqld",pid=4465,fd=21))
LISTEN   0        128                   [::]:22                 [::]:*       users:(("sshd",pid=751,fd=7))
# --> MariaDB utilise le port 3306
```
**Préparation de la base pour NextCloud**
```bash
[maximauve@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> Ctrl-C -- exit!
Aborted
```

**Exploration de la base de donnée**
```bash
[maximauve@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 21
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

MariaDB [(none)]> USE nextcloud
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```

#### C. Finaliser l'installation de NextCloud

* Sur mon PC:

```ps
PS C:\> type C:\Windows\System32\drivers\etc\hosts
#
127.0.0.1 localhost
::1 localhost
10.102.1.11 web.tp2.linux
```
Je peux me connecter à mon serveur nexcloud sur mon navigateur avec l'url `http://web.tp2.linux`

Je me reconnecte à ma db:
```sql
select table_schema as database_name,
    count(*) as tables
from information_schema.tables
where table_type = 'BASE TABLE'
      and table_schema not in ('information_schema', 'sys',
                               'performance_schema', 'mysql')
group by table_schema
order by table_schema;


+---------------+--------+
| database_name | tables |
+---------------+--------+
| nextcloud     |     87 |
+---------------+--------+
1 row in set (0.002 sec)

```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | *             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Donnée  | 3306        | 10.102.1.11   |