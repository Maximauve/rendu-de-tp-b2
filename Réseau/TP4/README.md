# TP 4

## I. Dumb Switch

### 3. SetUp Topologies

```bash
# PC1
PC1> ip 10.1.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

#PC2
PC2> ip 10.1.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0


# ping

#PC1
PC1> ping 10.1.1.2 -c 2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=5.950 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.860 ms

#PC2
PC2> ping 10.1.1.1 -c 2

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=4.975 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=5.630 ms
```

## II. VLAN
**Adressage**
```bash
#same Ip que dans le I. pour PC1 & PC2
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

#Pings:

#PC1
PC1> ping 10.1.1.2 -c 2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=11.372 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.887 ms

PC1> ping 10.1.1.3 -c 2

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=7.991 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=2.972 ms

#PC2
PC2> ping 10.1.1.1 -c 2

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=4.975 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=5.630 ms

PC2> ping 10.1.1.3 -c 2

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=7.397 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=4.219 ms

#PC3
PC3> ping 10.1.1.1 -c 2

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.305 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=13.787 ms

PC3> ping 10.1.1.2 -c 2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.323 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.301 ms
```
**Configuration des VLANs**

