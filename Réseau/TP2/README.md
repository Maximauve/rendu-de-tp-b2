# TP 2

- [TP 2](#tp-2)
	- [I. ARP](#i-arp)
		- [1. Échange ARP](#1-échange-arp)
			- [Générer des requêtes ARP](#générer-des-requêtes-arp)
		- [2. Analyse de trames](#2-analyse-de-trames)
	- [II. Routage](#ii-routage)
		- [1. Mise en place du routage](#1-mise-en-place-du-routage)
		- [2. Analyse des trames](#2-analyse-des-trames)
## I. ARP

### 1. Échange ARP

#### Générer des requêtes ARP

**Ping**

1ère VM:

```
[maximauve@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.966 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.91 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.782 ms
^C
--- 10.2.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 0.782/1.220/1.913/0.496 ms
```

2nd VM:

```
[maximauve@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.869 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=2.29 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=2.56 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 0.869/1.905/2.558/0.742 ms
```

**ARP:**

1ere VM:

```
[maximauve@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:a9:12:3b STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:47 DELAY
```

Adresse MAC de node2: `08:00:27:a9:12:3b`

2nd VM:

```
[maximauve@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:3b:72:39 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:47 DELAY
```

Adresse MAC de node1: `08:00:27:3b:72:39`

```
[maximauve@node1 ~]$ ip a
1: [...]
2: [...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3b:72:39 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3b:7239/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

```
[maximauve@node2 ~]$ ip a
1: [...]
2: [...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a9:12:3b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea9:123b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

On voit que les adresses MAC correspondent bien par rapport aux `ip n s`

### 2. Analyse de trames

Vider la table ARP: `sudo ip neigh flush all`

`sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22`
sauvegarde dans le fichier `tp2_arp.pcap`

Ouverture du port 8888: `firewall-cmd --add-port=8888/tcp`
Ouverture du serveur pour réccupérer le fichier: `python3 -m http.server 8888`

| ordre | type trame  | source                      | destination                   |
| ----- | ----------- | --------------------------- | ----------------------------- |
| 1     | Requête ARP | `node2` `08:00:27:a9:12:3b` | Broadcast `ff:ff:ff:ff:ff:ff` |
| 2     | Réponse ARP | `node1` `08:00:27:3b:72:39` | `node2` `08:00:27:a9:12:3b`   |
| 15    | Requête ARP | `node1` `08:00:27:3b:72:39` | `node2` `08:00:27:a9:12:3b`   |
| 16    | Réponse ARP | `node2` `08:00:27:a9:12:3b` | `node1` `08:00:27:3b:72:39`   |

## II. Routage

### 1. Mise en place du routage
* Activer le routage sur le noeud `router.net1.tp2`
On regarde la zone utilisée par le firewall:
```
[maximauve@router ~]$ sudo firewall-cmd --list-all
[sudo] password for maximauve:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[maximauve@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
```
On active le masquerading:
```
[maximauve@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[maximauve@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
* Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

Pour `node 1`:

Temporairement:
```
[maximauve@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[sudo] password for maximauve:
[maximauve@node1 ~]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 101
10.2.2.0/24 via 10.2.1.254 dev enp0s8
```
Définitivement:
```
sudo nano /etc/sysconfig/network-scripts/route-enp0s8
```
Puis je met `10.2.2.0/24 via 10.2.1.254 dev enp0s8` dans le fichier
Pour `marcel`:

Temporairement:
```
[maximauve@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[maximauve@marcel ~]$ ip route show
10.2.1.0/24 via 10.2.2.254 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 101
```
Définitivement:
```
sudo nano /etc/sysconfig/network-scripts/route-enp0s8
```
Puis je met `10.2.1.0/24 via 10.2.2.254 dev enp0s8` dans le fichier
**Test ping**

`node 1`:
```
[maximauve@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.02 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.15 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.30 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.146/1.486/2.015/0.380 ms
```
`marcel`:
```
[maximauve@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.23 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.22 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.29 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.221/1.248/1.290/0.030 ms
```

### 2. Analyse des trames

Ping depuis `node 1` vers `marcel`:
```
[maximauve@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.99 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.24 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.25 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.05 ms
64 bytes from 10.2.2.12: icmp_seq=5 ttl=63 time=1.22 ms
64 bytes from 10.2.2.12: icmp_seq=6 ttl=63 time=1.32 ms
64 bytes from 10.2.2.12: icmp_seq=7 ttl=63 time=1.19 ms
64 bytes from 10.2.2.12: icmp_seq=8 ttl=63 time=1.24 ms
64 bytes from 10.2.2.12: icmp_seq=9 ttl=63 time=1.38 ms
64 bytes from 10.2.2.12: icmp_seq=10 ttl=63 time=1.11 ms
^C
--- 10.2.2.12 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9019ms
rtt min/avg/max/mdev = 1.052/1.298/1.994/0.251 ms
```
On regarde les Tables arp:

`node 1`:
```
[maximauve@node1 ~]$ ip n s
10.2.1.254 dev enp0s8 lladdr 08:00:27:74:4b:42 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:47 DELAY
```
`marcel`:
```
[maximauve@marcel ~]$ ip n s
10.2.2.254 dev enp0s8 lladdr 08:00:27:ce:72:a9 STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:25 DELAY
```
`router`:
```
[maximauve@router ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:3b:72:39 STALE
10.2.2.12 dev enp0s9 lladdr 08:00:27:a9:12:3b STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:47 DELAY
```
On peut voir que `node 1` envoie un ping qui n'est pas dans son réseau, donc le ping va vers la passerelle par défaut, qui s'avère être `router`. `router` reçoit le ping, mais il voit qu'il n'est pas pour lui, donc le redirige vers `marcel`, quyi le reçoit bien.
Ensuite, même sens dans le chemin inverse.

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `AA:BB:CC:DD:EE`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel` `AA:AA:AA:AA:AA` | x              | `node1` `AA:BB:CC:DD:EE`   |
| ...   | ...         | ...       | ...                       |                |                            |
| ?     | Ping        | ?         | ?                         | ?              | ?                          |
| ?     | Pong        | ?         | ?                         | ?              | ?                          |