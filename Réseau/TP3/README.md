# TP 3
## I. Architecture réseau

### 1. Adressage

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | Adresse broadcast|
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`        | `255.255.255.192` | 62                           | `10.3.0.190`         | `10.3.0.191`                                                                                   |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                           | `10.3.0.126`         | `10.3.0.127`                                                                                   |
| `server2`     | `10.3.0.192`        | `255.255.255.240` | 14                           | `10.3.0.206`         | `10.3.0.207`                                                                                   |





| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`                                | `10.3.0.126/25`        | `10.3.0.206/28`        | Carte NAT             |
| ...          | ...                                            | ...                    | ...                    | `10.3.?.?/?`          |



### 2. Routeur

```bash
# Ip définie sur les 3 réseaux:
[maximauve@localhost ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f9:65:54 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef9:6554/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:41:34:7e brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe41:347e/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cc:16:3d brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fecc:163d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

# Accès Internet:
[maximauve@localhost ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.3 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 18.750/19.548/20.347/0.810 ms

# Résolution de noms:
[maximauve@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[...]
DNS1=1.1.1.1
[maximauve@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s9
[...]
DNS1=1.1.1.1
[maximauve@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s10
[...]
DNS1=1.1.1.1

# Hostname
[maximauve@localhost ~]$ sudo hostname router.tp3
[maximauve@localhost ~]$ echo "router.tp3" | sudo tee /etc/hostname
router.tp3
[maximauve@localhost ~]$ hostname
router.tp3

# Activation du routage
[maximauve@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  [maximauve@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s10 enp0s9 enp0s8 enp0s3
  [maximauve@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[maximauve@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

## II. Services d'Infra

### 1. Serveur DHCP

**Mettre en place un machine qui fera office de serveur DHCP**
```bash
# Hostname
[maximauve@localhost ~]$ sudo hostname dhcp.client1.tp3
[sudo] password for maximauve:
[maximauve@localhost ~]$ echo "dhcp.client1.tp3" | sudo tee /etc/hostname
dhcp.client1.tp3
[maximauve@localhost ~]$ hostname
dhcp.client1.tp3

# Partie DHCP
[maximauve@dhcp ~]$ sudo dnf -y install dhcp-server
[...]
[maximauve@dhcp ~]$ sudo vim /etc/dhcp/dhcpd.conf
# Voir fichier 📁dhcpd.conf
[maximauve@dhcp ~]$ sudo systemctl enable --now dhcpd
[sudo] password for maximauve:
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
[maximauve@dhcp ~]$ sudo firewall-cmd --add-service=dhcp
success
[maximauve@dhcp ~]$ sudo firewall-cmd --runtime-to-permanent
success
```

**Mettre en place un client dans le réseau `client1`**
```bash
# Hostname
[maximauve@localhost ~]$ sudo hostname marcel.client1.tp3
[sudo] password for maximauve:
[maximauve@localhost ~]$ echo "marcel.client1.tp3" | sudo tee /etc/hostname
marcel.client1.tp3
[maximauve@localhost ~]$ hostname
marcel.client1.tp3

# Accès Internet
[maximauve@marcel ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=18.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.1 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.397/19.253/20.109/0.856 ms

[maximauve@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 14220
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             161     IN      A       142.250.179.110

;; Query time: 19 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 17:18:34 CEST 2021
;; MSG SIZE  rcvd: 55
```

### 2. Serveur DNS
#### B. SETUP DNS

**Mettre en place une machine qui fera office de sevreur DNS**
```bash
# server1
[maximauve@dns1 ~]$ ip a
[...]
    link/ether 08:00:27:ba:95:ce brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.10/25 brd 10.3.0.127 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feba:95ce/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

# Hostname
[maximauve@dns1 ~]$ sudo hostname "dns1.server1.tp3"
[maximauve@localhost ~]$ echo "dns1.server1.tp3" | sudo tee /etc/hostname
dns1.server1.tp3
[maximauve@localhost ~]$ hostname
dns1.server1.tp3

# Ajouter un dns public connu
[maximauve@dns1 ~]$ cat /etc/resolv.conf
search server1.tp3
nameserver 8.8.8.8

# install bind
[maximauve@dns1 ~]$ sudo dnf install -y bind bind-utils
[...]
Complete!

# named.conf
[maximauve@dns1 ~]$ sudo cat /etc/named.conf
```
```conf
[...]
options {
        listen-on port 53 { any; };
        listen-on-v6 port 53 { any; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };
[...]
zone "server1.tp3" IN {
        type master;
        file "/var/named/server1.tp3.forward";
        allow-update { none; };
};
zone "server2.tp3" IN {
        type master;
        file "/var/named/server2.tp3.forward";
        allow-update { none; };
};
[...]
```
```bash
# server1.tp3.forward
[maximauve@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
[sudo] password for maximauve:
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804
         3600
         1800
         604800
         86400
)
@         IN  NS      dns1.server1.tp3
@         IN  A       10.3.0.10

; Set each IP address of a hostname. Sample A records.
dns1           IN  A       10.3.0.10
router         IN  A       10.3.0.126
[maximauve@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server2.tp3. root.server2.tp3. (
         2021080804
         3600
         1800
         604800
         86400
)
@         IN  NS      dns1.server2.tp3.
@         IN  A       10.3.0.10

; Set each IP address of a hostname. Sample A records.
dns1           IN  A       10.3.0.10
router         IN  A       10.3.0.126

# Mettre les bonnes permissions aux fichiers crées:
[maximauve@dns1 ~]$ sudo chown root:named /var/named/server1.tp3.forward
[maximauve@dns1 ~]$ sudo chmod 644 /var/named/server1.tp3.forward
[maximauve@dns1 ~]$ sudo chown root:named /var/named/server2.tp3.forward
[maximauve@dns1 ~]$ sudo chmod 644 /var/named/server2.tp3.forward

# Autorisations du firewall pour autoriser les services DNS
[maximauve@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[maximauve@dns1 ~]$ sudo firewall-cmd --reload
success
[maximauve@dns1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3
  sources:
  services: dns ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

# vérifier si tout est bon:
[maximauve@dns1 ~]$ sudo named-checkzone dns1.server1.tp3 /var/named/server1.tp3.forward
zone dns1.server1.tp3/IN: loaded serial 2021080804
OK
[maximauve@dns1 ~]$ sudo named-checkzone dns1.server1.tp3 /var/named/server2.tp3.forward
zone dns1.server1.tp3/IN: loaded serial 2021080804
OK
```

**Tester le DNS depuis `marcel1`**
```bash
# Définir manuellement l'utilisation de notre serveur DNS
[maximauve@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
DNS1=10.3.0.10

# Résolution de nom
[maximauve@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40580
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 0b65f355495584d71e999342615c7157f56920bc3fceb4e0 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.10

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 1 msec
;; SERVER: 10.3.0.10#53(10.3.0.10)
;; WHEN: Tue Oct 05 17:37:59 CEST 2021
;; MSG SIZE  rcvd: 103

[maximauve@marcel ~]$ dig router.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> router.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8029
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: da4d288dda96c5d24c040e35615c71b2528396691aa65c85 (good)
;; QUESTION SECTION:
;router.server1.tp3.            IN      A

;; ANSWER SECTION:
router.server1.tp3.     86400   IN      A       10.3.0.126

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; ADDITIONAL SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.10

;; Query time: 2 msec
;; SERVER: 10.3.0.10#53(10.3.0.10)
;; WHEN: Tue Oct 05 17:39:30 CEST 2021
;; MSG SIZE  rcvd: 126
```
On voit que en réponse des `dig`, à la fin, il y a `SERVER: 10.3.0.10#53(10.3.0.10)` ce qui montre bien l'adresse IP de mon serveur DNS

