# TP 1

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

#### En ligne de Commande:

_Infos des cartes réseau de mon pc_
Pour trouver les information dont j'ai besoin, j'utilise la commande `ipconfig /all`

Je peux donc retrouver:

- Ma carte réseau Wifi:

```
Carte réseau sans fil Wi-Fi :

    [...]
    Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
    Adresse physique . . . . . . . . . . . : C8-58-C0-27-22-FC
    [...]
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.234(préféré)
    [...]
```

N'ayant pas de port RJ45, aucune carte Ethernet ne figure dans la liste de mes cartes réseau.

_Mon gateway_
Toujours en utilisant la commande `ipconfig /all`, je peux voir la passerelle par défaut de ma carte WiFi:

```
Carte réseau sans fil Wi-Fi :

   [...]
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   [...]

```

---

#### En graphique

Pour accéder graphiquement aux information de ma carte WiFi, il me suffit de me rendre dans: `Panneau de configuration\Réseau et Internet\Connexions réseau`. Une fois sur cette interface, je fais un clic droit sur ma carte WiFi, puis statut, et enfin détails. Je peux maintenant voir tout ça:
![](./img/ip_mac_gateway.png)

#### A quoi sert la gateway dans le réseau d'Ynov?

La gateway dans le réseau d'Ynov sert à faire le lien entre le réseau d'Ynov et d'autres réseaux extérieux.

### 2. Modification des informations

#### A. Modification d'adresse IP

Pour modifier mon adresse IP, je me rend au même endroit que pour voir mes informations de ma carte WiFi (`Panneau de configuration\Réseau et Internet\Connexions réseau`) puis je fais clic droit sur ma carte WiFi -> Statut -> Propriétés:
Je peux maintenant modifier mon adresse IP:
![](./img/ip_edit.png)
J'ai changé uniquement le dernier octet.

Je me rend compte que je n'ai plus accès à internet. La raison est simple: l'adresse IP que j'ai choisie est déjà prise.

#### B. Table ARP

Pour afficher la table ARP, j'utilise la commande suivante:

```
C:\Users\maxmo>arp -a

Interface : 10.33.0.234 --- 0x2
  Adresse Internet      Adresse physique      Type
  10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
  10.33.0.167           2c-8d-b1-d9-6c-55     dynamique
  10.33.1.34            a8-7e-ea-3d-f7-25     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.62            dc-fb-48-c8-b1-4e     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.1.96            78-2b-46-e5-17-62     dynamique
  10.33.2.133           3c-f0-11-f7-dc-76     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.2.247           80-32-53-3b-f7-f9     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```

Je reconnais l'adresse MAC de la passerelle de mon réseau, car elle se situe à côté de l'adresse IP.

```
10.33.3.253           00-12-00-40-4c-bf     dynamique
```

Maintenant, je vais ping des adresses IP au hasard:

```
C:\Users\maxmo>ping 10.33.2.222

Envoi d’une requête 'Ping'  10.33.2.222 avec 32 octets de données :
Réponse de 10.33.2.222 : octets=32 temps=111 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=9 ms TTL=64

Statistiques Ping pour 10.33.2.222:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 9ms, Maximum = 111ms, Moyenne = 60ms
Ctrl+C
^C
C:\Users\maxmo>ping 10.33.3.219

Envoi d’une requête 'Ping'  10.33.3.219 avec 32 octets de données :
Réponse de 10.33.3.219 : octets=32 temps=93 ms TTL=64

Statistiques Ping pour 10.33.3.219:
    Paquets : envoyés = 1, reçus = 1, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 93ms, Maximum = 93ms, Moyenne = 93ms
Ctrl+C
^C
C:\Users\maxmo>ping 10.33.3.232

Envoi d’une requête 'Ping'  10.33.3.232 avec 32 octets de données :
Réponse de 10.33.3.232 : octets=32 temps=7 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=9 ms TTL=64

Statistiques Ping pour 10.33.3.232:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 7ms, Maximum = 9ms, Moyenne = 8ms
Ctrl+C
^C
```

Voyons voir ma table ARP maintenant:

```
C:\Users\maxmo>arp -a

Interface : 10.33.0.234 --- 0x2
  Adresse Internet      Adresse physique      Type
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
  10.33.0.167           2c-8d-b1-d9-6c-55     dynamique
  10.33.0.175           74-d8-3e-f2-cc-95     dynamique
  10.33.0.232           b4-6d-83-d2-d4-80     dynamique
  10.33.1.34            a8-7e-ea-3d-f7-25     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.62            dc-fb-48-c8-b1-4e     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.1.96            78-2b-46-e5-17-62     dynamique
  10.33.2.94            70-9c-d1-03-ec-4c     dynamique
  10.33.2.133           3c-f0-11-f7-dc-76     dynamique
  10.33.2.190           74-4c-a1-d8-e6-03     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.2.222           88-bf-e4-cb-d9-46     dynamique
  10.33.2.234           28-df-eb-fb-50-7c     dynamique
  10.33.2.247           80-32-53-3b-f7-f9     dynamique
  10.33.3.2             14-4f-8a-65-8c-c7     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.24            ac-12-03-2e-e4-92     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.219           a0-78-17-b5-63-bb     dynamique
  10.33.3.232           88-66-5a-4d-a7-f5     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```

Je peux repérer les trois adresses MAC des adresses IP que j'ai ping précédemment:

```
[...]
10.33.2.222           88-bf-e4-cb-d9-46     dynamique
[...]
10.33.3.219           a0-78-17-b5-63-bb     dynamique
10.33.3.232           88-66-5a-4d-a7-f5     dynamique
[...]
```

### C. `nmap`

Je commence par lancer un scan de ping sur le réseau d'Ynov:

```
C:\Users\maxmo>nmap -sP 10.33.0.0/22

Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 14:59 Paris, Madrid (heure d’été)
[...]
Nmap scan report for 10.33.3.226

Host is up (0.0040s latency).

MAC Address: F0:77:C3:06:8F:42 (Intel Corporate)

Nmap scan report for 10.33.3.229

Host is up (0.023s latency).

MAC Address: 94:08:53:46:75:D3 (Liteon Technology)

Nmap scan report for 10.33.3.232

Host is up (0.051s latency).

MAC Address: 88:66:5A:4D:A7:F5 (Apple)

Nmap scan report for 10.33.3.245

Host is up (0.76s latency).

MAC Address: EC:9B:F3:51:3B:1B (Samsung Electro-mechanics(thailand))

Nmap scan report for 10.33.3.250

Host is up (0.058s latency).

MAC Address: 14:F6:D8:43:45:95 (Intel Corporate)

Nmap scan report for 10.33.3.252

Host is up (0.0030s latency).

MAC Address: 00:1E:4F:F9:BE:14 (Dell)

Nmap scan report for 10.33.3.253
[...]
```

Et ma table ARP:

```
Interface : 10.33.0.234 --- 0x2
  Adresse Internet      Adresse physique      Type
  10.33.0.27            a8-64-f1-8b-1d-4d     dynamique
  10.33.0.59            e4-0e-ee-73-73-96     dynamique
  10.33.0.67            dc-f5-05-db-cc-ab     dynamique
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.75            62-77-19-80-6e-28     dynamique
  10.33.0.84            98-af-65-d7-82-f0     dynamique
  10.33.0.114           14-7d-da-24-1c-07     dynamique
  10.33.0.119           38-f9-d3-63-b1-0f     dynamique
  10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
  10.33.0.142           5c-3a-45-5f-48-5d     dynamique
  10.33.0.143           f0-18-98-41-11-07     dynamique
  10.33.0.147           e0-b5-5f-eb-20-87     dynamique
  10.33.0.157           26-57-f1-a3-f2-89     dynamique
  10.33.0.166           7e-7a-66-16-64-ef     dynamique
  10.33.0.167           2c-8d-b1-d9-6c-55     dynamique
  10.33.0.172           3a-3a-a7-0a-ea-88     dynamique
  10.33.0.175           74-d8-3e-f2-cc-95     dynamique
  10.33.0.184           12-2c-fd-7f-90-1f     dynamique
  10.33.0.195           12-b9-05-11-2c-54     dynamique
  10.33.0.232           b4-6d-83-d2-d4-80     dynamique
  10.33.1.34            a8-7e-ea-3d-f7-25     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.62            dc-fb-48-c8-b1-4e     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.1.96            78-2b-46-e5-17-62     dynamique
  10.33.1.125           08-f8-bc-6c-b6-02     dynamique
  10.33.1.140           fa-38-8f-3f-42-a0     dynamique
  10.33.1.154           34-36-3b-65-ad-d4     dynamique
  10.33.1.180           76-f1-a0-2c-c4-25     dynamique
  10.33.1.230           90-9c-4a-ba-6b-f3     dynamique
  10.33.2.3             e0-2b-e9-42-5e-91     dynamique
  10.33.2.22            f8-5e-a0-52-4f-36     dynamique
  10.33.2.42            16-d8-79-91-57-a4     dynamique
  10.33.2.64            90-9c-4a-ba-53-f6     dynamique
  10.33.2.79            a8-7e-ea-46-e7-8b     dynamique
  10.33.2.94            70-9c-d1-03-ec-4c     dynamique
  10.33.2.105           ec-2e-98-ca-da-e9     dynamique
  10.33.2.106           f6-6f-2f-27-39-50     dynamique
  10.33.2.133           3c-f0-11-f7-dc-76     dynamique
  10.33.2.160           de-e6-48-af-e5-d2     dynamique
  10.33.2.167           58-96-1d-14-ca-7b     dynamique
  10.33.2.179           3c-22-fb-7a-96-64     dynamique
  10.33.2.181           c2-c0-11-a0-65-ed     dynamique
  10.33.2.190           74-4c-a1-d8-e6-03     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.2.222           88-bf-e4-cb-d9-46     dynamique
  10.33.2.234           28-df-eb-fb-50-7c     dynamique
  10.33.2.237           be-fd-16-82-f4-05     dynamique
  10.33.2.244           64-c7-53-e5-0f-32     dynamique
  10.33.2.247           80-32-53-3b-f7-f9     dynamique
  10.33.3.2             14-4f-8a-65-8c-c7     dynamique
  10.33.3.3             a4-83-e7-61-01-74     dynamique
  10.33.3.13            26-7b-3f-46-6d-9e     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.24            ac-12-03-2e-e4-92     dynamique
  10.33.3.25            3c-a6-f6-36-1f-b5     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.61            96-fd-87-13-4b-ee     dynamique
  10.33.3.73            14-7d-da-55-4a-ef     dynamique
  [...]
```

### D. Modification d'adresse IP (part 2)

Pour trouver une adresse IP libre, je trouve une adresse IP qui ne figure pas dans ma commande `nmap`. Je vais donc prendre `10.33.0.154`

```
C:\Users\maxmo>nmap -sn 10.33.0.154

Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 16:31 Paris, Madrid (heure d’été)

Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn

Nmap done: 1 IP address (0 hosts up) scanned in 1.76 seconds
```

Aucun host n'est actif, je peux donc m'y connecter.

une fois connecté, je vais ping ma passerelle, ainsi que google.com pour m'assurer de bien avoir accès à internet:

```
C:\Users\maxmo>ipconfig

[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::c581:a752:f14:64c9%2
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.163
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]

C:\Users\maxmo>ping 10.33.3.253

Envoi d’une requête 'Ping'  10.33.3.253 avec 32 octets de données :
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=8 ms TTL=255

Statistiques Ping pour 10.33.3.253:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 8ms, Moyenne = 5ms
Ctrl+C
^C

C:\Users\maxmo>ping google.com

Envoi d’une requête 'ping' sur google.com [142.250.178.142] avec 32 octets de données :
Réponse de 142.250.178.142 : octets=32 temps=39 ms TTL=115
Réponse de 142.250.178.142 : octets=32 temps=39 ms TTL=115
Réponse de 142.250.178.142 : octets=32 temps=37 ms TTL=115
Réponse de 142.250.178.142 : octets=32 temps=39 ms TTL=115

Statistiques Ping pour 142.250.178.142:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 37ms, Maximum = 39ms, Moyenne = 38ms
```

## II. Exploration locale en duo

### 1. Modification d'adresse IP

Même principe que pour changer son adresse IP WifI, mais sur la carte Ethernet.

Je définis mon adresse IP comme ceci:

```
IPV4: 192.168.0.1
Masque: 255.255.255.252
Passerelle: 192.168.0.2
```

Et mon coéquipier comme cela:

```
IPV4: 192.168.0.2
Masque: 255.255.255.252
Passerelle: 192.168.0.1
```

On vérifie que les changements se sont bien appliqués:

```
C:\Users\maxmo>ipconfig

[...]
Carte Ethernet Ethernet 5 :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::44:f9e5:4fb3:a866%71
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.2
[...]
```

On peut bel et bien se ping entre nous:

```
C:\Users\maxmo>ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```

Ma table ARP se présente comme ça:

```
C:\Users\maxmo>arp -a

[...]
Interface : 192.168.0.1 --- 0x47
  Adresse Internet      Adresse physique      Type
  192.168.0.2           04-d4-c4-e6-15-34     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Je peux voir que j'ai bien eu une interraction avec 192.168.0.2 récemment.

### 2. Utilisation d'un des deux comme gateway

Une fois la démarche de partage de connexion par Ethernet effectué, on teste de ping des DNS ou google.com:

```
C:\Users\maxmo>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=42 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 30ms, Maximum = 42ms, Moyenne = 36ms
Ctrl+C
^C
C:\Users\maxmo>ping google.com

Envoi d’une requête 'ping' sur google.com [216.58.214.174] avec 32 octets de données :
Réponse de 216.58.214.174 : octets=32 temps=20 ms TTL=113
Réponse de 216.58.214.174 : octets=32 temps=18 ms TTL=113

Statistiques Ping pour 216.58.214.174:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 20ms, Moyenne = 19ms
```

J'ai bien accès à internet.

Grâce à la commande `tracert 1.1.1.1`, je peux voir le chemin que parcours mon paquet pour accéder au DNS `1.1.1.1`

```
C:\Users\maxmo>tracert 1.1.1.1

Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :

  1     2 ms     1 ms     1 ms  LAPTOP-UD8C09I0 [192.168.0.2]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     3 ms     2 ms     4 ms  10.33.3.253
  4   373 ms    45 ms    34 ms  10.33.10.254
  5     4 ms     2 ms     8 ms  reverse.completel.net [92.103.174.137]
  6    10 ms    10 ms    11 ms  92.103.120.182
  7    20 ms    19 ms    21 ms  172.19.130.117
  8    19 ms    19 ms    19 ms  46.218.128.74
  9    18 ms    18 ms    39 ms  equinix-paris.cloudflare.com [195.42.144.143]
 10    18 ms    19 ms    18 ms  one.one.one.one [1.1.1.1]

Itinéraire déterminé.
```

### 3. Création d'un chat privé

Sur le pc de mon coéquipier, il tape la commande suivante:

```
C:\Users\matte\netcat>nc.exe -l -p 8888
```

De mon côté, j'effectue ceci:

```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe 192.168.0.2 8888
Test depuis 192.168.0.1
Test reçu depuis 192.168.0.2
```

On peut voir que la connexion s'est bien effectuée, nous avons pu échanger quelques mots, depuis nos deux machines.

**--> Pour aller plus loin:**
Cette fois ci, c'est moi qui héberge le serveur. J'effectue la commande suivante:

```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe -l -p 9999
```

Tandis que mon camarade s'est connecté grâce à cette commande:

```
C:\Users\matte\netcat>nc.exe 192.168.0.1 9999
```

Et le résultat:

```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe -l -p 9999 192.168.0.2
Test depuis l'hôte
Test reçu depuis le guest
```

### 4. FireWall

#### Autoriser les ping

On se rend dans les paramètres avancés de notre pare-feu windows.

- On créé une nouvelle règle de traffic entrant.
- Type de règle -> Personalisée
- Programme -> Tous les programmes
- Protocole et ports -> ICMPv4
  -> Perso... -> Certains types ICMP -> requête d'echo -> type 8
- Étendue -> Toutes adresses IP
- Action -> Autoriser la connexion
- Profil -> Privé
  Je la nomme `Autorisation Ping aller`
  Pour le retour, comme ce n'est pas le même type de requête d'echo, je refais le même preocédé sauf pour la requête d'echo où je la met de **type 0**.
  Je la nomme `Autorisation Ping retour`

Nous vérifions si on tente de se ping, cela fonctionne bien:

```
C:\Users\maxmo>ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```

#### Autoriser le traffic sur le port qu'utilise `nc`

Pareil que précédemment, on va créer une nouvelle règle de traffic entrant:

- Type de règle -> Port
- Protocole et ports -> TCP / Ports locaux spécifiques `(8888)`
- Action -> Autoriser la connexion
- Profil -> Privé

Nous allons à nouveau tenter de se parler avec le chat privé de `nc`:

```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe -l -p 8888 192.168.0.2
Test guest
Test bien reçu depuis l'hôte
```

Tout fonctionne parfaitement.

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

Pour pouvoir trouver l'adresse IP du serveur DHCP d'Ynov, on utilise tout simplement `ipconfig /all`

```
C:\Users\maxmo>ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

   [...]
   Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 18:24:10
   Bail expirant. . . . . . . . . . . . . : vendredi 17 septembre 2021 20:24:09
   [...]
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   [...]
```

J'ai accès à l'adresse IP et au bail du serveur DHCP. Je vois que j'ai obtenu mon bail à 18h24 et qu'il expire à 20h24.
